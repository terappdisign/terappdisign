import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AboutPage } from '../about/about';
import { RegistrePage} from '../registre/registre';
import { LoginPage} from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  addItem(){
    this.navCtrl.setRoot(AboutPage)
  }

  public registre(){
    this.navCtrl.push(RegistrePage);
}

public login(){
    this.navCtrl.push(LoginPage);
}

}
