import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReservationPage } from '../reservation/reservation';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController) {

  }
  goReservationPage(){
    this.navCtrl.setRoot(ReservationPage);
  }
}
